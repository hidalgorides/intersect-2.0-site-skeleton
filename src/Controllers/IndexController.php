<?php

namespace App\Controllers;

use Intersect\Core\AbstractController;
use Intersect\Http\Response\TwigResponse;

class IndexController extends AbstractController {

    public function index()
    {
        return new TwigResponse('welcome.twig', [
            'currentDate' => date('F j, Y, g:i:s a')
        ]);
    }

}