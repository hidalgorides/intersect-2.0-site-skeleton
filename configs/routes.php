<?php

use Intersect\Http\Route;

return [
    Route::get('/', 'App\Controllers\IndexController#index')
];
