<?php

// see following url for example configs to override
// https://bitbucket.org/hidalgorides/intersect-2.0-backend/src/master/configs/base-registry.php

return [
    'classes' => [],
    'singletons' => [],
    'events' => [],
    'commands' => []
];
