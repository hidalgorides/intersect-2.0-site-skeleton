<?php

error_reporting(E_ALL);

require_once dirname(__FILE__) . '/../vendor/autoload.php';

use \Intersect\Application,
    \Intersect\Http\Request,
    \Intersect\Http\RequestHandler;

$request = Request::initFromGlobals();

$application = Application::instance();
$application->setBasePath(realpath(__DIR__ . '/../'));
$application->init();

$requestHandler = new RequestHandler($application);
$response = $requestHandler->execute($request);

$requestHandler->finish($request, $response);
